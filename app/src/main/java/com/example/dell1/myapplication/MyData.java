package com.example.dell1.myapplication;

/**
 * Created by dell 1 on 10/21/2015.
 */
public class MyData {
    public String myTitle;
    public int myNum;
    public MyData(){
        super();
    }

    public MyData(String myTitle, int myNum){
        super();
        this.myTitle = myTitle;
        this.myNum = myNum;

    }

}
