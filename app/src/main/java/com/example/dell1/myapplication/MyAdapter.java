package com.example.dell1.myapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell 1 on 10/21/2015.
 */
public class MyAdapter extends ArrayAdapter<MyData> {
    private Context context;
    private int resource;
    private MyData[] objects;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=  ((Activity) context).getLayoutInflater();
        View row=inflater.inflate(resource,parent,false);
        TextView title= (TextView) row.findViewById(R.id.title);
        TextView number=(TextView) row.findViewById(R.id.number);
        title.setText((CharSequence) objects[position].myTitle);
        number.setText(Integer.toString(objects[position].myNum));
        return row;
    }

    public MyAdapter(Context context, int resource, MyData[] objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
}
